#pragma once
#include <iostream>
#include <list>

class Table
{

protected:
	static const std::string myFile;
	static const std::string insert;
	static const std::string values;
	static const std::string endbracket;
	const std::string tableName;

	std::list<std::string> listOfAtr;
	std::list<std::string> listOfVal;
public:
	
	Table(std::string name);
	bool virtual Insert();

	virtual ~Table( ) = 0;
};

class Autor : public Table
{
public:
	static const std::string tableName;

	Autor(std::string name);
};

class Wydawnictwo : public Table
{
public:
	static const std::string tableName;

	Wydawnictwo(std::string name);
};

class Gatunek : public Table
{
public:
	static const std::string tableName;

	Gatunek(std::string name);
};


class Ksiazka : public Table
{
public:
	static const std::string tableName;

	Ksiazka(std::string name);
};


class KsiazkaAutor : public Table
{
public:
	static const std::string tableName;

	KsiazkaAutor(std::string name);
};


class Czytelnik: public Table
{
public:
	static const std::string tableName;

	Czytelnik(std::string name);
};


class Wypozyczenie : public Table
{
public:
	static const std::string tableName;

	Wypozyczenie(std::string name);
};
