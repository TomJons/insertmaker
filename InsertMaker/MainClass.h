#pragma once
#include <iostream>
#include <exception>
#include "Table.h"

class MainClass
{
private:
	int tableNumber;
	bool errorFlag;
	std::string errorMessage;
	Table* pTable;

public:
	MainClass();
	~MainClass();
	
	void hello();
	bool moreData();
	bool insertion();
	void showErrors();
	void chooseTable();
	bool getErrorFlag();
};
