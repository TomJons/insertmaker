#include <iostream>
#include <fstream>
#include "Table.h"

using namespace std;

//inicjalizacja skladnikow statycznych klas
const string Table::myFile = "insert.sql";

const string Autor::tableName = "autorzy";
const string Wydawnictwo::tableName = "wydawnictwa";
const string Gatunek::tableName = "gatunki";
const string Ksiazka::tableName = "ksiazki";
const string KsiazkaAutor::tableName = "ksiazki_autorzy";
const string Czytelnik::tableName = "czytelnik";
const string Wypozyczenie::tableName = "wypozyczenia";


const string Table::insert = "INSERT INTO ";
const string Table::values = " VALUES(";
const string Table::endbracket = ");\r\n";


Table::Table(string name) : tableName(name)
{
	;
}


Table::~Table()
{
}

//metoda odpowiedzialna za wrzucenie insertu do pliku 
//oraz pobierajaca dane od uzytkownika
bool Table::Insert()
{
	try
	{
		list<string>::iterator iter = listOfAtr.begin();
		listOfVal.clear();
		while (iter != listOfAtr.end())
		{
			char* temp = new char[255];
			cout << "Wpisz " << (*iter).c_str() << endl;
			cin.getline(temp, 255, '\n');
			listOfVal.push_back(temp);
			delete[] temp;
			iter++;
		}
		ofstream file(myFile, ios::app);

		iter = listOfVal.begin();
		string row = insert + tableName + values;
		while (iter != listOfVal.end())
		{
			row += '\'';
			row += (*iter);
			row += '\'';
			if ((++iter) != listOfVal.end())
				row += ',';
		}
		row += endbracket;

		file << row.c_str();
		file.close();
	}
	catch (...)
	{
		cout << "blad";
		return false;
	}
	return true;
}


Autor::Autor(string name) : Table(name)
{
	listOfAtr.push_back("id");
	listOfAtr.push_back("imie");
	listOfAtr.push_back("nazwisko");
	listOfAtr.push_back("narodowosc");
}

Wydawnictwo::Wydawnictwo(string name) : Table(name)
{
	listOfAtr.push_back("id");
	listOfAtr.push_back("nazwa");
}

Gatunek::Gatunek(string name) : Table(name)
{
	listOfAtr.push_back("id");
	listOfAtr.push_back("nazwa");
}

Ksiazka::Ksiazka(string name) : Table(name)
{
	listOfAtr.push_back("id");
	listOfAtr.push_back("tytul");
	listOfAtr.push_back("id_gat");
	listOfAtr.push_back("id_wyd");
	listOfAtr.push_back("cena");
	listOfAtr.push_back("liczba_stron");
	listOfAtr.push_back("rok_wydania");
	listOfAtr.push_back("ilosc");
}

KsiazkaAutor::KsiazkaAutor(string name) : Table(name)
{
	listOfAtr.push_back("id");
	listOfAtr.push_back("id_ksiazki");
	listOfAtr.push_back("id_autora");
}

Czytelnik::Czytelnik(string name) : Table(name)
{
	listOfAtr.push_back("id");
	listOfAtr.push_back("imie");
	listOfAtr.push_back("nazwisko");
	listOfAtr.push_back("adres");
	listOfAtr.push_back("email");
}

Wypozyczenie::Wypozyczenie(string name) : Table(name)
{
	listOfAtr.push_back("id");
	listOfAtr.push_back("id_czyt");
	listOfAtr.push_back("id_ks");
}
