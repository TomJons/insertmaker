#include "MainClass.h"

using namespace std;

int  main() 
{
	MainClass* mc = new MainClass();

	for( ; ; )
	{
		mc->hello();
		mc->chooseTable();
		mc->insertion();

		if (mc->getErrorFlag())
		{
			mc->showErrors();
			break;
		}

		if (mc->moreData())
			continue;
		else
			break;
	}

	delete mc;
	return 0;
}
