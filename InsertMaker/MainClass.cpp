#include <iostream>
#include "MainClass.h"
#include "Table.h"
using namespace std;

//konstruktor
MainClass::MainClass()
{
	tableNumber = -1;
	errorFlag = false;
	errorMessage = "";
	pTable = NULL;
}


//destruktor
MainClass::~MainClass()
{
	delete pTable;
	pTable = NULL;
}


//funkcja wyswietlajaca powietanie 
void MainClass::hello()
{
	cout << "Witamy w Insert Makerze" << endl;
	cout << "Wprowadz nr tabeli:" << endl
		<< "1 - Autorzy" << endl
		<< "2 - Wydawnictwa" << endl
		<< "3 - Gatunki" << endl
		<< "4 - Ksiazki" << endl
		<< "5 - KsiazkiAutorzy" << endl
		<< "6 - Czytelnicy" << endl
		<< "7 - Wypozyczenia" << endl;
	cin >> tableNumber;
}


//funkcja pytajaca czy chcemy wiecej wpisow
bool MainClass::moreData()
{
	char more;
	cout << "Chcesz wpisac wiecej danych do bazy? t = tak , n = nie " << endl;
	cin >> more;
	if (more == 't')
		return true;
	else if (more == 'n')
		return false;
	else
		return true;
}


//funkcja dodajaca inserty 
bool MainClass::insertion() 
{
	int recordsNumber;
	cout << "Ile rekordow chcesz wpisac do bazy? (1-100)" << endl;
	cin >> recordsNumber;
	cin.ignore();

	if (recordsNumber > 0 && recordsNumber < 101)
	{
		for (int i = 0; i < recordsNumber; i++)
		{
			try
			{
				pTable->Insert();
			}
			catch (exception ex)
			{	
				errorMessage += ex.what() + '\n';
				errorFlag = true;
			}
		}
		return true;
	}
	else
	{
		cerr << "Wpisana wartosc nie misci sie w przedziale (1-100)";
		return false;
	}
}


//funkcja pokazujaca errrory jakie wystapily w czasie dzialania programu 
void MainClass::showErrors()
{
	cerr << "Wystapily bledy podczas dodawania insertow:" << endl
		<< errorMessage.c_str();
}


//metoda do wybierania tabeli 
void MainClass::chooseTable()
{
	switch (tableNumber)
	{
	case 0:
		pTable = new Autor(Autor::tableName);
		break;

	case 1:
		pTable = new Wydawnictwo(Wydawnictwo::tableName);
		break;

	case 2:
		pTable = new Gatunek(Gatunek::tableName);
		break;

	case 3:
		pTable = new Ksiazka(Ksiazka::tableName);
		break;

	case 4:
		pTable = new KsiazkaAutor(KsiazkaAutor::tableName);
		break;

	case 5:
		pTable = new Czytelnik(Czytelnik::tableName);
		break;

	case 6:
		pTable = new Wypozyczenie(Wypozyczenie::tableName);
		break;

	default:
		cout << "Blad nieodnaleziono podanego numeru" << endl;
		break;
	}
}


//metoda do pobierania flagi bledu
bool MainClass::getErrorFlag()
{
	return errorFlag;
}
