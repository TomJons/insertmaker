create table autorzy(
  id number not null primary key,
  imie varchar2(128),
  nazwisko varchar2(128),
  narodowosc varchar2(128)
  );
  
  
create table wydawnictwa(
  id number not null primary key,
  nazwa varchar2(255)
  );
  
 create table gatunki(
  id number not null primary key,
  nazwa varchar2(255)
  ); 
  
create table ksiazki(
  id number not null primary key,
  tytul varchar2(255),
  id_gat number,
  id_wyd number,
  cena number,
  liczba_stron number,
  rok_wydania number,
  ilosc number,
  
  constraint fk_ks_gat foreign key (id_gat) references gatunki (id),
  constraint fk_ks_wyd foreign key (id_wyd) references wydawnictwa (id)
  );
  

create table ksiazki_autorzy(
  id number not null primary key,
  id_ksiazki number,
  id_autora number,
  
  constraint fk_ksau_ks foreign key (id_ksiazki) references ksiazki (id),
  constraint fk_ksau_au foreign key (id_autora) references autorzy (id)
  );


create table czytelnik(
  id number not null primary key,
  imie varchar2(128),
  nazwisko varchar2(128),
  adres varchar2(128),
  email varchar2(128)
  );
  
  create table wypozeczenia(
   id number not null primary key,
   id_czyt number, 
   id_ks number,

	constraint fk_wyp_czyt foreign key (id_czyt) references czytelnik (id),
	constraint fk_wyp_ks foreign key (id_ks) references ksiazki (id)
   );
   
